import { Controller, Post, Body, HttpException, HttpStatus } from '@nestjs/common';
import { UserService } from '../../../common/user/user.service';
import { LoginDTO } from '../../../common/user/dto/login.dto';

@Controller('v1/user')
export class UserController {
  constructor(
    private readonly userService: UserService,
  ) {}

  @Post('login')
  login(@Body() loginDTO: LoginDTO): object {
    const token = this.userService.login(loginDTO);

    if (!token) {
      throw new HttpException({
        statusCode: HttpStatus.UNAUTHORIZED,
        error: 'Unauthorized',
      }, HttpStatus.UNAUTHORIZED);
    }

    return token;
  }
}
