import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from '../../../common/user/user.service';
import { UserTokenService } from '../../../common/user/token/user-token.service';

describe('UserController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const mockAppModule: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService, UserTokenService],
    }).compile();

    app = mockAppModule.createNestApplication();
    await app.init();
  });

  it('Auth by user', async () => {
    return await request(app.getHttpServer())
      .post('/v1/user/login')
      .send({ email: 'user@user.ru', password: '87654321' })
      .expect(201);
  });

  it('Auth with wrong data', async () => {
    return await request(app.getHttpServer())
      .post('/v1/user/login')
      .send({ email: 'bad@user.ru', password: '12345678' })
      .expect(401);
  });
});
