import { Injectable } from '@nestjs/common';
import { User } from './user';
import { UserTokenService } from './token/user-token.service';
import { LoginDTO } from './dto/login.dto';

@Injectable()
export class UserService {
  private users = [
    new User('admin@admin.ru', '12345678'),
    new User('user@user.ru', '87654321'),
  ];

  constructor(
    private readonly userTokenService: UserTokenService,
  ) {}

  login({ email, password } : LoginDTO): object {
    const user = this.users.find(user => user.email === email && user.password === password);

    if (!user) return;

    const token = this.userTokenService.getToken();
    return { token };
  }
}
