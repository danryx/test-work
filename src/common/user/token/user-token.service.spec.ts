import { Test, TestingModule } from '@nestjs/testing';
import { UserTokenService } from './user-token.service';

describe('UserTokenService', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [UserTokenService],
    }).compile();
  });

  describe('getToken', () => {
    it('should return random token', () => {
      const userTokenService = app.get<UserTokenService>(UserTokenService);
      expect(userTokenService.getToken().length).toBe(32);
    });
  });
});
