import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { UserTokenService } from './token/user-token.service';

describe('UserService', () => {
  let app: TestingModule;
  let userService: UserService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [UserService, UserTokenService],
    }).compile();

    userService = app.get<UserService>(UserService);
  });

  describe('login', () => {
    it('Auth by user', () => {
      expect(userService.login({ email: 'user@user.ru', password: '87654321' })).toEqual(expect.objectContaining({
        token: expect.any(String)
      }));
    });

    it('Auth with empty email', () => {
      expect(userService.login({ email: '', password: '87654321' })).toBeUndefined();
    });

    it('Auth with empty password', () => {
      expect(userService.login({ email: 'user@user.ru', password: '' })).toBeUndefined();
    });
  });
});
